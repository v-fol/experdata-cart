from django.urls import path
from . import views


urlpatterns = [
    path('order/', views.OrderView.as_view(), name='get_or_create_order'),
    path('order/item/', views.ItemView.as_view(), name='create_or_delete_item'),
    path('order/cost/', views.OrderCostView.as_view(), name='set_order_price'),
    path('order/item/quantity/', views.ItemQuantityView.as_view(),
         name='change_item_quantity'),

]
