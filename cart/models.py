from django.db import models
from customer.models import Customer
from product.models import Product


class Order(models.Model):
    customer_id = models.ForeignKey(
        Customer, on_delete=models.SET_NULL, null=True, blank=True)
    total_cost = models.FloatField(default=0)
    total_coupon = models.PositiveIntegerField(default=10)
    total_cost_with_coupons = models.FloatField(default=0)

    def __str__(self):
        return str(self.pk)


class Item(models.Model):
    product_id = models.ForeignKey(
        Product, on_delete=models.SET_NULL, blank=True, null=True)
    order_id = models.ForeignKey(
        Order, on_delete=models.SET_NULL, null=True, blank=True)
    quantity = models.PositiveIntegerField(default=1)
    date_added = models.DateTimeField(auto_now_add=True)
    next_item_coupon = models.PositiveIntegerField(null=True, blank=True)

    def __str__(self):
        return str(self.pk)
