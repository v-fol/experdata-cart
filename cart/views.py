from rest_framework.response import Response
from rest_framework import status
from rest_framework.views import APIView

from .models import Order, Item
from customer.models import Customer
from product.models import Product

from .serializer import OrderSerializer


class OrderView(APIView):

    def get(self, request):
        customer_id = request.GET.get('uuid')
        order_id = request.GET.get('order_id')

        customer = Customer.objects.get(uuid=customer_id)

        order = Order.objects.get(customer_id=customer, pk=order_id)

        serializer = OrderSerializer(order)
        return Response(data=serializer.data, status=status.HTTP_200_OK)

    def post(self, request):
        customer_id = request.data['uuid']

        customer = Customer.objects.get(uuid=customer_id)

        order = Order(customer_id=customer)
        order.save()

        return Response(status=status.HTTP_201_CREATED)


class ItemView(APIView):

    def post(self, request):
        customer_id = request.data['uuid']
        order_id = request.data['order_id']
        product_id = request.data['product_id']

        customer = Customer.objects.get(uuid=customer_id)

        order = Order(customer_id=customer, pk=order_id)
        product = Product(pk=product_id)

        items = Item.objects.filter(order_id=order)

        # Checking item quantity to add next item coupon.
        if items.count() >= 1:
            item = Item(order_id=order, product_id=product,
                        next_item_coupon=20)
            item.save()
        else:
            item = Item(order_id=order, product_id=product)
            item.save()

        return Response(status=status.HTTP_201_CREATED)

    def delete(self, request):
        customer_id = request.data['uuid']
        order_id = request.data['order_id']
        item_id = request.data['item_id']

        customer = Customer.objects.get(uuid=customer_id)

        order = Order(customer_id=customer, pk=order_id)

        item = Item.objects.get(order_id=order_id, pk=item_id)
        item.delete()

        items = Item.objects.filter(order_id=order)

        # Deleting next item coupon if the deleted item was first.
        if item_id == items.first() and items.count >= 1:
            first_item = items.first()
            first_item.next_item_coupon = 0
            first_item.save()

        return Response(status=status.HTTP_200_OK)


class ItemQuantityView(APIView):

    def put(self, request):
        customer_id = request.data['uuid']
        order_id = request.data['order_id']
        item_id = request.data['item_id']
        direction = request.GET.get('direction')

        customer = Customer.objects.get(uuid=customer_id)

        order = Order(customer_id=customer, pk=order_id)

        item = Item.objects.get(order_id=order, pk=item_id)

        if direction == 'add':
            item.quantity = item.quantity + 1
        elif direction == 'remove' and item.quantity > 1:
            item.quantity = item.quantity - 1
        elif direction == 'remove' and item.quantity == 1:
            return Response(status=status.HTTP_409_CONFLICT)
        else:
            return Response(status=status.HTTP_400_BAD_REQUEST)

        return Response(status=status.HTTP_200_OK)


class OrderCostView(APIView):

    def put(self, request):
        customer_id = request.data['uuid']
        order_id = request.data['order_id']

        customer = Customer.objects.get(uuid=customer_id)
        order = Order.objects.get(customer_id=customer, pk=order_id)
        items = Item.objects.filter(order_id=order)

        price_list = list()
        discount_price_list = list()

        # Iteration of the order items to get the price.
        for item in items:
            price = item.product_id.price

            # Check if the item product has a discount,
            # if true - calculating the discount,
            # if not - asigning base price.
            if item.product_id.product_coupon != None:
                discount_price = (price/100) * \
                    (100 - item.product_id.product_coupon)
            else:
                discount_price = price

            # If the item has a next_coupon - calculating the end price.
            if item.next_item_coupon != None:
                discount_price = (discount_price/100) * \
                    (100 - item.next_item_coupon)

            price_list.append(price * item.quantity)
            discount_price_list.append(discount_price * item.quantity)

        # Summing all the prices.
        order.total_cost = sum(price_list)
        order.save()
        # Summing all the discount prices and adding the total coupon.
        order.total_cost_with_coupons = (
            sum(discount_price_list)/100)*(100 - order.total_coupon)
        order.save()

        return Response(status=status.HTTP_200_OK)
