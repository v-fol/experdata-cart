from django.test import TestCase
from rest_framework.test import APIClient
from customer.models import Customer
from cart.models import Order, Item
from product.models import Product


class DBInstances():
    def create_customer(uuid):
        customer = Customer(uuid=uuid)
        customer.save()
        return customer

    def create_order(customer_id):
        order = Order(customer_id=customer_id)
        order.save()
        return order

    def create_product(slug, name, description, price, product_coupon=0):
        product = Product(slug=slug, name=name, description=description,
                          price=price, product_coupon=product_coupon)
        product.save()
        return product

    def create_item(order_id, product_id, quantity=1, next_item_coupon=0):
        item = Item(order_id=order_id, product_id=product_id,
                    quantity=quantity, next_item_coupon=next_item_coupon)
        item.save()
        return item


class APITests(TestCase):

    def setUp(self):
        self.client = APIClient()
        self.test_server_base_url = 'http://testserver/api'
        self.user_uuid = '6ec0bd7f-11c0-43da-975e-2a8ad9ebae0b'
        self.customer_id = DBInstances.create_customer(self.user_uuid)
        self.order = DBInstances.create_order(self.customer_id)
        self.product = DBInstances.create_product(
            slug='2c5ea4c0-4067-11e9-8bad-9b1deb4d3b7d', name='test_name', description='test_description', price=12)
        self.item = DBInstances.create_item(self.order, self.product)

    def test_create_order(self):
        response = self.client.post(
            f'{self.test_server_base_url}/order/', data={'uuid': self.user_uuid}, format='json')
        self.assertEqual(response.status_code, 201)

    def test_add_item(self):
        response = self.client.post(f'{self.test_server_base_url}/order/item/', data={
                                    'uuid': self.user_uuid, 'order_id': self.order.pk, 'product_id': self.product.pk}, format='json')
        self.assertEqual(response.status_code, 201)

    def test_delete_item(self):
        response = self.client.delete(f'{self.test_server_base_url}/order/item/', data={
                                      'uuid': self.user_uuid, 'order_id': self.order.pk, 'item_id': self.item.pk}, format='json')
        self.assertEqual(response.status_code, 200)

    def test_change_item_quantity(self):
        response = self.client.put(f'{self.test_server_base_url}/order/item/quantity/?direction=add', data={
                                   'uuid': self.user_uuid, 'order_id': self.order.pk, 'item_id': self.item.pk}, format='json')
        self.assertEqual(response.status_code, 200)

    def test_set_order_price(self):
        product = DBInstances.create_product(slug='2c5ea4c0-4067-11e9-8bad-9b1deb4d3b7f',
                                             name='test_name2', description='test_description', price=24, product_coupon=10)
        DBInstances.create_item(
            order_id=self.order, product_id=product, quantity=3, next_item_coupon=20)

        response = self.client.put(f'{self.test_server_base_url}/order/cost/', data={
                                   'uuid': self.user_uuid, 'order_id': self.order.pk}, format='json')
        self.assertEqual(response.status_code, 200)

    def test_get_order(self):
        response = self.client.get(
            f'{self.test_server_base_url}/order/?uuid={self.user_uuid}&order_id={self.order.pk}',  format='json')
        self.assertEqual(response.status_code, 200)
