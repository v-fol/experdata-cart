from django.db import models


class Customer(models.Model):
    uuid = models.UUIDField(null=True, blank=True)

    def __str__(self):
        return str(self.uuid)
