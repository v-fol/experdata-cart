from django.test import TestCase
from rest_framework.test import APIClient
# Create your tests here.


class APITests(TestCase):
    def setUp(self):
        self.client = APIClient()
        self.user_uuid = '6ec0bd7f-11c0-43da-975e-2a8ad9ebae0b'
        self.test_server_base_url = 'http://testserver/api'

    def test_customer_creation(self):
        response = self.client.post(f'{self.test_server_base_url}/customer/', data={'uuid': self.user_uuid}, format='json')
        self.assertEqual(response.status_code, 201)