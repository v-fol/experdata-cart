from rest_framework import response
from rest_framework.response import Response
from rest_framework import status
from rest_framework.views import APIView

from .models import Customer


class CustomerView(APIView):

    def post(self, request):
        uuid = request.data['uuid']

        customer = Customer(uuid=uuid)
        customer.save()

        return Response(status=status.HTTP_201_CREATED)