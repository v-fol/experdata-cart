# Experdata Cart (task - 2)

![UML](https://i.ibb.co/R2P2BBM/Cart-DB-1.png)
[UML Diagram](https://lucid.app/lucidchart/e1065694-12e9-4ad2-98f2-82adecc58c42/edit?viewport_loc=-196%2C-122%2C2740%2C1368%2C0_0&invitationId=inv_db2704f1-6cb8-4988-b402-dd2918ee44dc)

## Available Scripts

In the project directory, you can run:

### `set enviroment variable SECRET_KEY at venv - APIENV` - details at app/settings.py
### `pip install -r requirements.txt` to install packages
### `python manage.py makemigrations` to make migrations
### `python manage.py migrate` to migrate
### `python manage.py createsuperuser` to create superuser (for http://127.0.0.1:8000/admin/ access)
### `python manage.py runserver` to open dev server at http://127.0.0.1:8000/
### `python manage.py test` to run tests

## API Methods
### - base url - - http://127.0.0.1:8000/api
<pre>
Method  URI                                           DATA
POST    /customer/                                    data={'uuid'}
POST    /order/                                       data={'uuid'}
POST    /order/item/                                  data={'uuid', 'order_id', 'product_id'}
DELETE  /order/item/                                  data={'uuid', 'order_id', 'item_id'}
PUT     /order/item/quantity/?direction={direction}   data={'uuid', 'order_id', 'item_id'}   direction=['add', 'remove']
PUT     /order/price/                                 data={'uuid', 'order_id'}
GET     /order/?uuid={uuid}&order_id={order.pk}
</pre>
