from django.db import models
import uuid


class Product(models.Model):
    slug = models.UUIDField(default=uuid.uuid4, editable=False)
    name = models.CharField(max_length=22)
    description = models.CharField(max_length=200)
    price = models.FloatField()
    product_coupon = models.PositiveIntegerField(null=True, blank=True)

    def __str__(self):
        return str(self.slug)