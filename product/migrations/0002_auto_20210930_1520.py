# Generated by Django 3.2.7 on 2021-09-30 12:20

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('product', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='product',
            name='price',
            field=models.FloatField(default=1),
            preserve_default=False,
        ),
        migrations.AlterField(
            model_name='product',
            name='product_coupon',
            field=models.PositiveIntegerField(blank=True, null=True),
        ),
    ]
